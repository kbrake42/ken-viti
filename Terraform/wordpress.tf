data "aws_ami" "amazon-linux-2" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-x86_64-gp2"]
  }

  owners = ["amazon"]
}

data "template_file" "user_data" {
  template = file("userdata.sh")
  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  }
}

resource "aws_db_subnet_group" "wp-subnet-group" {
  name       = "wp-subnetgroup"
  subnet_ids = ["subnet-0b0f25121480d649a", "subnet-0b43071665614eff9"]

  tags = {
    Name = "Education"
  }
}

resource "aws_security_group" "rds" {
  name   = "rds-security-group"
  vpc_id = "vpc-09f4d0e849a933233"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = ["${aws_security_group.instance-sg.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "instance-sg" {
  name   = "instance-security-group"
  vpc_id = "vpc-09f4d0e849a933233"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


}

resource "aws_instance" "WordPress" {
 ami = data.aws_ami.amazon-linux-2.id
 instance_type = "t2.micro"
 subnet_id = "subnet-045ae14f10261ae8b"
 key_name = "procore-us-east-1"
 vpc_security_group_ids = [
              "${aws_security_group.instance-sg.id}"
            ]
 user_data = data.template_file.user_data.rendered
 tags = {
   Name = "WordPress"
 }

}

resource "aws_db_instance" "wordpressdb" {
 allocated_storage = 20
 identifier = "database-2"
 storage_type = "gp2"
 engine = "mysql"
 engine_version = "8.0.28"
 instance_class = "db.t2.micro"
 name = var.database_name
 username = var.database_user
 password = var.database_password
 db_subnet_group_name = aws_db_subnet_group.wp-subnet-group.name
 vpc_security_group_ids = [aws_security_group.rds.id]
 publicly_accessible    = false
 skip_final_snapshot    = true
 tags = {
    Name = "ExampleRDSServerInstance"
}
}

output "instance_ip_addr" {
  value = aws_instance.WordPress.public_ip
}
